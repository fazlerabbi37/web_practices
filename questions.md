Kindly do only what is asked in the tasks. Do not do anything extra. Consider that what is not said is not required.

You can use images hosted online. No need to download images offline to use.

Please use Git, and commit after every task is completed at least. If you think more commits are necessary or better in any task, please do so.

### Task 1

Create a simple web page, without any visible content.

### Task 2

Create:

- an unordered list with 2 items (put "item1", "item2" as content), 
- a table with 2 rows and 2 columns (put "c1r1", "c2r1", etc. as content).

### Task 3

- Add a random image which will be 100% width of the screen.
- Center the image horizontally.

### Task 4

- Inside the 2nd item of the already-created list, create a list with 3 items.
- Create a table inside the 2nd item of the nested list.
- In the newly-created table, add a random image of fixed size: `200px` height and `300px` width.

### Task 5

- Add an HTML `id` attribute of value `nestedList` to the nested list created in the last task.
- In the 3rd item of `nestedList`, add a hyperlink, which –
  - should show the content "HTML tag list".
  - should show the full form of the abbreviation "HTML" when the word "HTML" is hovered.
  - should redirect to this link: `https://html.com/tags`.